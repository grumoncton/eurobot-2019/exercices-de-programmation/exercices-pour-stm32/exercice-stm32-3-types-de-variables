// vim: spelllang=fr, keymap=accents
#include <mbed.h>

Serial serial(USBTX, USBRX, NULL, 9600);


// Il existe beaucoup de types de variables dans le langage C.  Cet exercice
// vous introduira aux types de variables qu'on utilise le plus souvent. Il est
// non seulement important de connaitre les types de variables, mais aussi de
// savoir quand utiliser lequel.


/**
 * Partie A: Définir des variables avec des types données
 *
 * Ajouter la définition de la variable demandée après chacun des commentaires
 * suivants
 */

// Définir une variable booléenne nommée `isMatchStarted` avec la valeur
// représentant faux.
bool isMatchStarted = false;

// Définir un nombre entier de 16 bits signé nommé `temperature` avec la valeur
// `-18`
int16_t temperature = -18;

// Définir un nombre entier de 16 bits non signé nommé `max16` avec la valeur
// `-1`
uint16_t max16 = -1;
// 0 -> 127
// 0b00000000 -> 0b01111111

// -128 -> -1
// 0b10000000 -> 0b11111111

// Définir un nombre entier de 8 bits signé nommé `noOverflow` avec la valeur
// `-128`
int8_t noOverflow = -128;

// Définir un nombre entier de 8 bits signé nommé `overflow` avec la valeur
// `+128`
int8_t overflow = 128;

// Définir un nombre à virgule flottante de double précision nommé `xPosition`
// avec la valeur `0.0`
float xPosition = 0.0;

typedef enum {
	PURPLE = 0,
	ORANGE = 1,
} StartingColour;

typedef enum {
	SPADES = 0,
	HEARTS = 1,
	CLUBS = 2,
	DIAMONDS = 3,
} Suit;

// Définir un `StartingColour` nommé `startingColour` avec la valeur `ORANGE`
StartingColour startingColour = ORANGE;


/**
 * Partie B: Choisir le type approprié
 *
 * Après chacun des commentaires suivants, définir une variable avec un type
 * approprié
 */

// Définir ci-dessous la variable `i` utilisée dans la boucle. Ne la donner pas
// une valeur.
uint8_t i;
const char abc[] = { 'a', 'b', 'c' };
void printABC() {
	for (i = 0; i < sizeof(abc) / sizeof(char); i++) {
		serial.printf("%c\n", abc[i]);
	}
}

// Définir la variable `timestamp` qui sera utilisée afin de stocker le nombre
// de secondes depuis le 1 janvier, 1970.
uint64_t timestamp;

// Définir la variable `yPosition` qui sera utilisée afin de stocker la position
// dans l'axe des y du centre du robot en mm.
float yPosition = 0.0;


/**
 * Partie C: Définition des fonctions
 *
 * Après chacun des commentaires suivants, définir la fonction demandée
 */

// Définir la fonction `printInt` qui prend comme argument un nombre entier
// signé de 64 bits et l'imprime sur le port série avec un retour de ligne.
void printInt(int64_t num) {
	serial.printf("%ld\n", (long int) num);
}



// Définir la fonction `addInts` qui prend comme arguments deux nombre entiers
// signés de 64 bits et retourne leur somme.
int64_t addInts(int64_t a, int64_t b) {
	return a + b;
}



/**
 * Partie D: Imprimer les résultats
 *
 * Imprimer les variables définies. Essayer d'expliquer les résultats.
 */

int main() {

	// Partie A: Donner le format qui permet d'imprimer les valeurs spécifiées
	serial.printf(
			"isMatchStarted: %s, temperature: %d, max16: %d, noOverflow: %d, overflow: %d, xPosition: %4.4f, startingColour: %d\n",
			(isMatchStarted ? "true" : "false"),
			temperature,
			max16,
			noOverflow,
			overflow,
			xPosition,
			startingColour
		);

	// Partie B: exécuter la fonction `printABC`
	printABC();

	// Partie C: Imprimer sur le port série la somme de `1` et `2`
	// (utilisez les fonctions définies)
	serial.printf("1 + 2 = ");
	serial.printf("%ld\n", (long int) addInts(1, 2));

	return 0;
}
