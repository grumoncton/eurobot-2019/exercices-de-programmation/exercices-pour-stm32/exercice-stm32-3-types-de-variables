<!--
vim: spelllang=fr keymap=accents
-->
# Exercice 3 de programmation pour STM32 : Types de variables et définition des fonctions

Il existe beaucoup de types de variables dans le langage C.  Cet exercice vous
introduira aux types de variables qu'on utilise le plus souvent. Il est non
seulement important de connaitre les types de variables, mais aussi de savoir
quand utiliser lequel.

## Marches à suivre

### Téléchargement
Exécutez les commandes suivantes dans *Git Bash*.
1. Créez un dossier pour le répertoire:
	```
	mkdir -p ~/code/grum/eurobot2019/exercices/exercices-pour-stm32
	```
1. Clonez le répertoire:
	```
	git clone ssh://gitlab@gitlab.robitaille.host:49/grum/eurobot2019/exercices/exercices-pour-stm32/exercice-stm32-3-types-de-variables.git ~/code/grum/eurobot2019/exercices/exercices-pour-stm32/exercice-stm32-3-types-de-variables
	```
1. Ouvrez le dossier:
	```
	cd ~/code/grum/eurobot2019/exercices/exercices-pour-stm32/exercice-stm32-3-types-de-variables
	```
1. Ouvrez le dossier dans *Visual Sutdio Code*:
	```
	code .
	```

### Programmation
Ouvrez le fichier `src/main.cpp` et suivez les directives.

### Compilation
Une fois que le code est écrit, compiler le programme avec la commande
`PlatformIO: Build` (tapez <kbd>ctrl-shift-p</kbd> pour ouvrir la ligne de
commande) ou avec le raccourci <kbd>ctrl-alt-b</kbd>.

### Téléversement
Si vous avez accès à un microcontrôleur (on en a commandé!), téléversez le
programme compilé avec la commande `PlatformIO: Upload` (tapez
<kbd>ctrl-shift-p</kbd> pour ouvrir la ligne de commande) ou avec le raccourci
<kbd>ctrl-alt-u</kbd>.
